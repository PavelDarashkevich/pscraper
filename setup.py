from setuptools import setup, find_packages

setup(
    name='psraper',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['pscraper = pscraper.__main__:main']
    }
)

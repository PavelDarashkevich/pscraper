from abc import abstractmethod
from datetime import datetime

from pscraper.core.database import models
from pscraper.core.database.database_manager import DatabaseManager


class DatabaseBatchPipeline(object):
    batch_size = 1000
    data = []

    def __init__(self, model, database_manager):
        self.model = model
        self.database_manager = database_manager

    @abstractmethod
    def build_record(self, item, spider):
        pass

    def open_spider(self, spider):
        self.flush()

    def close_spider(self, spider):
        self.flush()

    def process_item(self, item, spider):
        record = self.build_record(item, spider)
        if len(self.data) == self.batch_size:
            self.flush()
        self.data.append(record)
        return item

    def flush(self):
        if not self.data:
            return
        with self.database_manager.get_context():
            with self.database_manager.database.atomic():
                iq = self.model.insert_many(self.data)
                iq = iq.upsert()
                iq.execute()
        self.data = []


class MySqlGetLinksPipeline(DatabaseBatchPipeline):
    model = models.Articles
    resource = None

    def __init__(self, model, database_manager):
        super().__init__(model, database_manager)

    @classmethod
    def from_crawler(cls, crawler):
        db_config = crawler.settings.get('PSCRAPER_CONFIG').db_config

        return cls(models.Articles, DatabaseManager(db_config))

    def open_spider(self, spider):
        super(MySqlGetLinksPipeline, self).open_spider(spider)

        with self.database_manager.get_context():
            self.resource, _ = models.Resources.get_or_create(module=spider.name, domain=spider.domain)

    def build_record(self, item, spider):
        article = {
            'resource': self.resource,
            'url': item['url'],
            'posted_at': datetime.fromtimestamp(item['posted_at']),
            'http_response_status': item['response_status'],
            'updated_at': datetime.now()
        }
        return article


class MySqlParseLinksPipeline(object):
    def open_spider(self, spider):
        pass

    def close_spider(self, spider):
        pass

    def process_item(self, item, spider):
        article = item['article']

        if 'content' not in item:  # invalid links doesn't have a content
            return item

        publication, _ = models.Publications.get_or_create(article=article)

        for item_author in item.get('authors', default=[]):
            author, _ = models.Authors.get_or_create(name=item_author, resource=item['resource'])
            author.save()

            author_pub, _ = models.AuthorsPublications.get_or_create(publication=publication, author=author)
            author_pub.save()  # for each author

        category, _ = models.Categories.get_or_create(name=item.get('category', ''))
        category.save()

        item_keywords = item.get('keywords', default=[])
        for item_keyword in item_keywords:
            keyword, _ = models.Keywords.get_or_create(name=item_keyword)
            keyword.save()

            keyword_pub, _ = models.KeywordsPublications.get_or_create(publication=publication, keyword=keyword)
            keyword_pub.save()

        publication.authors_text = ','.join(item.get('authors', []))
        publication.category_text = item.get('category')
        publication.category = category
        publication.content = item.get('content', '')
        publication.lead = item.get('lead')
        publication.meta_tags = item.get('meta_tags')
        publication.keywords_text = ','.join(item.get('keywords', []))
        publication.posted_at = item.get('posted_at')
        publication.resource = item['resource']
        publication.title = item['title']
        publication.url = item['url_last']

        publication.link_count = item.get('link_count')
        publication.image_count = item.get('image_count')
        publication.video_count = item.get('video_count')

        if publication.created_at is None:
            publication.created_at = datetime.now()
        publication.updated_at = datetime.now()
        article.updated_at = datetime.now()

        publication.save()

        article.state = "parse_content_success"
        article.save()

        return item

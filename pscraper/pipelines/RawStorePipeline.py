import os
import pickle


class RawStorePipeline(object):
    def __init__(self):
        self.items = []

    def open_spider(self, spider):
        pass

    def close_spider(self, spider):
        self._flush()

    def process_item(self, item, spider):
        self.items.append(dict(item))
        print(len(self.items))
        if len(self.items) > 300000:
            self._flush()

    def _flush(self):
        if len(self.items) == 0:
            return

        with open(self._get_file_name(), "wb") as f:
            pickle.dump(self.items, f, pickle.HIGHEST_PROTOCOL)
        self.items.clear()

    @staticmethod
    def _get_file_name():
        id = 1
        while os.path.exists("out{}.bb".format(id)):
            id += 1

        return "out{}.bb".format(id)

import json


class JsonFileStorePipeline(object):
    def __init__(self, filename='articles.json'):
        self.filename = filename
        self.items = []

    def open_spider(self, spider):
        pass

    def close_spider(self, spider):
        with open(self.filename, 'w') as f:
            f.write(json.dumps(self.items))

    def process_item(self, item, spider):
        self.items.append(dict(item))

import lxml
import re
from lxml.html.clean import Cleaner


def remove_tags(content):
    if content is None or len(content.strip()) == 0:
        return ""

    cleaner = Cleaner(javascript=True, style=True, comments=True)
    content = cleaner.clean_html(lxml.html.fromstring(content)).text_content()

    content = re.sub(r"[ \t]{2,}", ' ', content)  # replace 2 or more spaces with one
    content = re.sub(r"[\s]{2,}", '\n', content)  # replace 2 or more whitespaces with one newline

    return content.strip()

import os


def absolute_path(path):
    return os.path.abspath(os.path.expandvars(os.path.expanduser(path)))

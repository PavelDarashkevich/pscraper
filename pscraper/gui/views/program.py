import PyQt5
import sys
from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtWidgets import QWidget, QMainWindow, QApplication


class ProgramView(QMainWindow):
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        self.initUI()

    def initUI(self):
        self.statusBar().showMessage('Ready')

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Statusbar')

        # todo add mode (get links or/and parse links)
        # todo checkbox pipelines (with move up/down priority)
        #   todo pipeline options
        # todo checkbox modules (onliner/tutby)
        # todo output mode (debug, info, error, etc)
        # todo other config: max requests, obey robots.txt


def tt():
    print("Hello")

if __name__ == '__main__':
    app = QApplication(sys.argv)

    program_view = ProgramView(None)
    program_view.show()
    sys.exit(app.exec())

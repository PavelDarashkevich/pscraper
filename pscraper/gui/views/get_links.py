import os

import PyQt5
import sys
from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi


class GetLinksView(QMainWindow):
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        self.initUI()

    def initUI(self):
        module_name = sys.modules[__name__].__file__
        module_dir = os.path.dirname(module_name)
        loadUi(os.path.abspath(os.path.join(module_dir, '../ui/GetLinks.ui')), self)


        # todo add mode (get links or/and parse links)
        # todo checkbox pipelines (with move up/down priority)
        #   todo pipeline options
        # todo checkbox modules (onliner/tutby)
        # todo output mode (debug, info, error, etc)
        # todo other config: max requests, obey robots.txt


def tt():
    print("Hello")

if __name__ == '__main__':
    app = QApplication(sys.argv)

    program_view = GetLinksView(None)
    program_view.show()
    sys.exit(app.exec())

import argparse


class ArgumentsParser(object):
    def __init__(self):
        pass

    @staticmethod
    def parse_arguments():
        parser = argparse.ArgumentParser()
        parser.add_argument('--get_links', action='store_true', default=False)
        parser.add_argument('--parse_links', action='store_true', default=False)
        parser.add_argument('--gui', action='store_true')
        parser.add_argument('--modules', nargs='+', type=str)
        parser.add_argument('--config', type=str)
        args = parser.parse_args()

        return args
import json

from pscraper.core.config import Config
from pscraper.utils.file_system import absolute_path


class ConfigManager(object):
    def __init__(self):
        pass

    def load(self, path):
        config = Config()
        if path is None:
            return config

        path = absolute_path(path)
        with open(path) as f:
            json_data = json.load(f)

        dict_params = {}
        main_params = {}
        for key in json_data.keys():
            if isinstance(json_data[key], dict):
                dict_params[key] = json_data[key]
            else:
                main_params[key] = json_data[key]

        self._update_attributes(config, main_params)

        self._update_attributes(config.db_config, dict_params['database'])
        self._update_attributes(config.scraper_config, dict_params['scraper'])

        return config

    def save(self, path):
        raise NotImplementedError()
        with open(path, 'w') as f:
            self.config.write(f)

    @staticmethod
    def _update_attributes(config, params):
        for param_key in params.keys():
            if not hasattr(config, param_key):
                raise ValueError('Invalid param: %s' % param_key)

            setattr(config, param_key, params[param_key])


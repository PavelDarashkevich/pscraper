from pscraper.arguments_parser import ArgumentsParser
from pscraper.config_manager import ConfigManager
from pscraper.core.database import database_manager
from pscraper.core.get_links import get_links
from pscraper.core.parse_links import parse_links


def main():
    args = ArgumentsParser().parse_arguments()
    config = ConfigManager().load(args.config)

    config.modules = args.modules

    if args.get_links:
        get_links(args.modules, config)

    if args.parse_links:
        for module_name in args.modules:
            parse_links(module_name, config)


if __name__ == '__main__':
    main()

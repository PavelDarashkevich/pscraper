import datetime

import scrapy
from iso8601 import iso8601
from pscraper.core.database import models
from pscraper.core.items import ArticlesItem, PublicationsItem
from scrapy.loader import ItemLoader

from pscraper.core.spiders import GetLinksSpider, ParseLinksSpider


class TutbyArticlesSpider(GetLinksSpider):
    name = 'tut_by'
    domain = 'https://tut.by'

    headers = {
        "Content-Type": "application/json"
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def start_requests(self):
        d = datetime.datetime(2000, 1, 1)
        while d < datetime.datetime.now():
            url = "https://news.tut.by/archive/{}.{}.{}.html".format(d.day, d.month, d.year)
            yield scrapy.Request(url, callback=self.parse, headers=self.headers)
            d += datetime.timedelta(days=1)

    def parse(self, response):
        for article in response.css('.lists__li'):
            url = article.xpath('a/@href').extract_first()

            if not self.is_valid_article_url(url):
                continue

            posted_at = int(article.xpath('@data-tm').extract_first())
            updated_at = article.xpath('@data-update-tm').extract_first()
            response_status = response.status
            yield ArticlesItem(
                url=url,
                posted_at=posted_at,
                response_status=response_status
            )

    @staticmethod
    def is_valid_article_url(url):
        if 'tut.by' not in url:
            return False

        urls_mask = ['blog.tut.by',
                     'jobs.tut.by',
                     'help.tut.by',
                     'vashdom.tut.by']

        if any(mask in url for mask in urls_mask):
            return False
        return True


class TutbyPublicationsSpider(ParseLinksSpider):
    name = 'article'
    module = 'tutby'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.resource = models.Resources.select(models.Resources.id).where(
            models.Resources.module == self.module).get()

    def start_requests(self):
        articles = models.Articles.select(models.Articles.id, models.Articles.url).where(
            models.Articles.resource ==
            self.resource)
        for article in articles:
            if 'tut.by' not in article.url:
                continue
            yield scrapy.Request(article.url, meta={'article': article})

    def get_author(self, response):
        # authors = response.xpath('//*[itemprop="author"]/*[itemprop="name"]/text()').extract()
        authors = response.xpath('//span[@itemprop="name"]/text()').extract()
        return [author.strip() for author in authors]

    @staticmethod
    def get_date(response):
        try:
            dt = response.xpath('//*[@itemprop="datePublished"]/@datetime').extract_first()
            return iso8601.parse_date(dt)
        except iso8601.ParseError:
            return None

    @staticmethod
    def get_content(response):
        return response.xpath('//*[@itemprop="articleBody"]').extract_first()

    @staticmethod
    def get_headline(response):
        element = response.xpath('//*[@itemprop="headline"]')
        comments_element = element.css('.b-comment_badge')
        if comments_element:
            comments_element = comments_element[0].root
            comments_element.getparent().remove(comments_element)
        return element.extract_first()

    @staticmethod
    def get_categories(response):
        article_sections = response.xpath('//*[@itemprop="articleSection"]')
        article_sections_urls = article_sections.xpath("@href").extract()

        article_sections_text = article_sections.xpath("text()").extract()
        article_sections_text += article_sections.xpath("@content").extract()

        return ','.join(text.strip() for text in article_sections_text)

    @staticmethod
    def get_tags(response):
        # tags_text = response.css('.b-article-info-tags').xpath(
        #     'a/text()').extract()  # todo get tags more correctly
        tags_text = response.css('.b-article-info-tags').xpath('./*/a/text()').extract()

        return [text.strip() for text in tags_text]

    @staticmethod
    def get_lead(response):
        return response.xpath('//*[@itemprop="articleBody"]/p/strong').extract_first()

    @staticmethod
    def get_meta_tags(response):
        return response.xpath('//meta[@name="news_keywords"]/@content').extract_first()

    def parse(self, response):
        if 'redirect_urls' in response.request.meta:
            url_start = response.request.meta['redirect_urls'][0]
        else:
            url_start = response.url

        il = ItemLoader(item=PublicationsItem())
        il.add_value('resource', self.resource)

        il.add_value('article', response.meta['article'])
        il.add_value('authors', self.get_author(response))
        il.add_value('category', self.get_categories(response))
        il.add_value('content', self.get_content(response))
        il.add_value('lead', self.get_lead(response))
        il.add_value('meta_tags', self.get_meta_tags(response))
        il.add_value('keywords', self.get_tags(response))
        il.add_value('posted_at', self.get_date(response))
        il.add_value('title', self.get_headline(response))
        il.add_value('url_start', url_start)
        il.add_value('url_last', response.url)
        il.add_value('image_count', self.get_image_cnt(response))
        il.add_value('link_count', self.get_link_cnt(response))
        il.add_value('video_count', self.get_video_cnt(response))
        yield il.load_item()

    @staticmethod
    def get_image_cnt(response):
        return 0  # todo

    @staticmethod
    def get_link_cnt(response):
        return 0

    @staticmethod
    def get_video_cnt(response):
        return 0

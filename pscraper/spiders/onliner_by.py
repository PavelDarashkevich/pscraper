import datetime
import re
import urllib.parse

import scrapy
from scrapy.loader import ItemLoader

from pscraper.core.items import ArticlesItem, PublicationsItem
from pscraper.core.spiders import GetLinksSpider, ParseLinksSpider


class OnlinerArticlesSpider(GetLinksSpider):
    name = 'onliner_by'
    domain = 'https://onliner.by'

    start_urls = [
        'https://people.onliner.by/?fromDate=999999999999',
        'https://auto.onliner.by/?fromDate=999999999999',
        'https://tech.onliner.by/?fromDate=999999999999',
        'https://realt.onliner.by/?fromDate=999999999999',
    ]

    headers = {
        "Content-Type": "application/json"
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, headers=self.headers)

    def parse(self, response):
        uri = urllib.parse.urlsplit(response.url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=uri)

        articles = response.css('.news-tidings__list .news-tidings__item')
        for article in articles:
            url = article.css('.news-tidings__stub').xpath('@href').extract_first()
            if url is None:
                url = article.css('.news-tiles__stub').xpath('@href').extract_first()

            posted_at = article.xpath('@data-post-date').extract_first()
            response_status = response.status
            if url is None or posted_at is None:
                raise ValueError()
            posted_at = int(posted_at)

            item = ArticlesItem(
                url=urllib.parse.urljoin(domain, url),
                posted_at=posted_at,
                response_status=response_status)

            yield item

        from_date = response.css('.news-tidings__list .news-tidings__item').xpath('@data-post-date').extract()[-1]
        next_page = '{}?fromDate={}'.format(domain, from_date)
        yield scrapy.Request(next_page, callback=self.parse, headers=self.headers)


class OnlinerPublicationsSpider(ParseLinksSpider):
    name = 'article'
    module = "onliner"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.resource = kwargs['resource']

    def start_requests(self):
        articles = Articles.select(Articles.id, Articles.url).where(Articles.resource == self.resource)
        for article in articles:
            yield scrapy.Request(article.url, meta={'article': article})

    def get_author(self, response):
        text = response.css('.news-header__author::text').extract_first()
        creators = self.process_creators(text)
        return [creators['автор']] if 'автор' in creators else []

    @staticmethod
    def process_creators(source):
        s = source.replace('&nbsp', ' ').strip()
        res = {'other': []}
        pairs = s.split('. ')
        for pair in pairs:
            if ': ' not in pair:
                res['other'].append(pair)
                continue
            a, b = pair.split(': ', maxsplit=1)
            res[a.strip().lower()] = b.strip()
        return res

    @staticmethod
    def get_date(response):
        url = response.url.split('/')
        day = int(url[-2])
        month = int(url[-3])
        year = int(url[-4])
        time = response.css('.news-header__time')[0].root.text
        match = re.search(r'(\d{1,2}):(\d{2})', time)
        hour, minute = match.groups()
        hour, minute = int(hour), int(minute)
        return datetime.datetime(year, month, day, hour, minute)

    def get_lead(self, response):
        lead_elements = response.css('.news-entry .news-entry__speech')
        if lead_elements:
            return lead_elements[0].root.text.strip()
        return None

    @staticmethod
    def get_content(response):
        content = response.css('.news-text')
        # removes title and head from DOM, may cause bugs.
        exclude = content.css('.news-header__title')
        exclude += content.css('.news-entry__speech')
        for el in exclude:
            el = el.root
            el.getparent().remove(el)

        return content[0].extract()

    @staticmethod
    def get_headline(response):
        return response.css('.news-header__title::text').extract_first().strip()

    @staticmethod
    def get_categories(response):
        categories_urls = response.css('.news-reference__item_primary').xpath('a/@href').extract()
        categories_text = response.css('.news-reference__item_primary').xpath('a/text()').extract()

        return ','.join(text.strip() for text in categories_text)

    @staticmethod
    def get_tags(response):
        tags_urls = response.css('.news-reference__item_secondary').xpath('a/@href').extract()
        tags_text = response.css('.news-reference__item_secondary').xpath('a/text()').extract()

        return [text.strip() for text in tags_text]

    @staticmethod
    def get_images_cnt(response):
        return len(response.css('.news-media__preview').extract())

    def parse(self, response):
        if 'redirect_urls' in response.request.meta:
            url_start = response.request.meta['redirect_urls'][0]
        else:
            url_start = response.url
        il = ItemLoader(item=PublicationsItem())

        il.add_value('resource', self.resource)

        il.add_value('article', response.meta['article'])
        il.add_value('authors', self.get_author(response))
        il.add_value('lead', self.get_lead(response))
        il.add_value('category', self.get_categories(response))
        il.add_value('title', self.get_headline(response))
        il.add_value('meta_tags', "")
        il.add_value('keywords', self.get_tags(response))
        il.add_value('posted_at', self.get_date(response))
        il.add_value('url_start', url_start)
        il.add_value('url_last', response.url)
        il.add_value('content', self.get_content(response))
        il.add_value('image_count', self.get_images_cnt(response))
        il.add_value('link_count', 0)
        il.add_value('video_count', 0)
        yield il.load_item()

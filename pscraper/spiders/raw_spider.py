import json
import zlib

import scrapy

from pscraper.core.database import models
from pscraper.core.spiders import GetLinksSpider, SpiderType


class RawSpider(GetLinksSpider):
    name = 'article'
    spider_type = SpiderType.GET_LINKS

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def start_requests(self):
        with open('downloaded_links.json') as f:
            downloaded_links = json.load(f)

        links_set = set(downloaded_links)

        articles = models.Articles.select(models.Articles.url)
        indices = list(range(len(articles)))
        # random.shuffle(indices)
        for index in indices:
            article = articles[index]
            if article.url in links_set:
                continue

            if index % 10 == 0:
                print(index)
            yield scrapy.Request(article.url, meta={'article': article})

    def parse(self, response):
        if 'redirect_urls' in response.request.meta:
            url_start = response.request.meta['redirect_urls'][0]
        else:
            url_start = response.url

        yield {
            "url_start": url_start,
            "url": response.url,
            "content": zlib.compress(response.body, 9)
        }


class Config(object):
    def __init__(self):
        self.has_gui = False
        self.modules = []
        self.program_type = None
        self.scraper_config = ScraperConfig()
        self.db_config = DatabaseConfig()


class ScraperConfig(object):
    def __init__(self):
        self.obey_robots_txt = True
        self.concurrent_requests = 32
        self.cookies_enabled = True


class DatabaseConfig(object):
    def __init__(self):
        self.user_name = None
        self.password = None
        self.db_name = None

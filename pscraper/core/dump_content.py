import datetime
import os
import pickle
import zlib
from scrapy import Request
from scrapy.crawler import CrawlerProcess
from scrapy.http import HtmlResponse

from onliner import models
from onliner.pipelines.MySqlPipelines import MySqlParseLinksPipeline
from onliner.spiders.RawSpider import RawSpider
from onliner.spiders.Onliner import OnlinerPublicationsSpider
from onliner.spiders.Tutby import TutbyPublicationsSpider


def parse_raw_links(spider, module_name):
    settings = {'NEWSPIDER_MODULE': 'onliner.spiders',
                'SPIDER_MODULES': ['onliner.spiders'],
                'ITEM_PIPELINES': {
                    "onliner.pipelines.RawStorePipeline.RawStorePipeline": 100
                },
                'BOT_NAME': 'onliner',
                'LOG_LEVEL': 'INFO',
                'ROBOTSTXT_OBEY': True,
                'CONCURRENT_REQUESTS': 128}

    process = CrawlerProcess(settings)
    process.crawl(spider, resource='')
    process.start()
    process.stop()


def cache_iter():
    # cache_files = [f for f in os.listdir(os.getcwd()) if os.path.isfile(os.path.join(os.getcwd(), f)) and '.bb' in f]
    cache_files = ['out13.bb']

    for cache_file in cache_files:
        with open(cache_file, 'rb') as f:
            data = pickle.load(f)

            for row in data:
                yield row
    return


def parse_from_cache():
    itemproc = MySqlParseLinksPipeline()
    itemproc.open_spider(None)

    onlinerSpider = OnlinerPublicationsSpider(resource=models.Resources.select(models.Resources.id).where(
        models.Resources.module == 'onliner').get())
    tutbySpider = TutbyPublicationsSpider(resource=models.Resources.select(models.Resources.id).where(
        models.Resources.module == 'tutby').get())

    # article_from_url = {article.url: article for article in models.Articles.select().limit(15000).offset(9900)}
    print('----')

    articles_count = models.Articles.select().count()

    article_from_url = {article.url: article for article in models.Articles.select()}
    processed_rows = 0
    for row in cache_iter():
        url_start = row['url_start']
        current_article = article_from_url.get(url_start, None)
        if current_article is None:
            continue

        processed_rows += 1
        if processed_rows % 100 == 0:
            print(datetime.datetime.now(), processed_rows)
        url = row['url']
        body = zlib.decompress(row['content'])

        req = Request(url=url, meta={
            'redirect_urls': [url_start],
            # 'article': models.Articles.get(url=url_start)
            'article': current_article
        })

        r = HtmlResponse(url=url, body=body)
        r.status_code = 200
        r.request = req

        current_spider = None
        if 'onliner.by' in r.url:
            current_spider = onlinerSpider
        if 'tut.by' in r.url:
            current_spider = tutbySpider
        if current_spider is None:
            print("Invalid url: " + r.url)
            continue

        for item in current_spider.parse(r):
            itemproc.process_item(item, None)

    itemproc.close_spider(None)


if __name__ == '__main__':
    # get_links_from_dump first of all!!!

    # parse_raw_links(RawSpider, '')
    parse_from_cache()
    pass

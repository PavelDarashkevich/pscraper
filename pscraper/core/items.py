import scrapy
from scrapy.loader.processors import MapCompose, Join, TakeFirst, Identity

from pscraper.utils.html_cleaner import remove_tags


class ArticlesItem(scrapy.Item):
    url = scrapy.Field()
    posted_at = scrapy.Field()
    response_status = scrapy.Field()


class PublicationsItem(scrapy.Item):
    article = scrapy.Field(
        output_processor=TakeFirst()
    )
    authors = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Identity()
    )
    category = scrapy.Field(
        output_processor=TakeFirst()
    )

    content = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Join()
    )
    lead = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Join()
    )
    title = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Join()
    )
    image_count = scrapy.Field(
        output_processor=TakeFirst()
    )
    link_count = scrapy.Field(
        output_processor=TakeFirst()
    )
    meta_tags = scrapy.Field(
        output_processor=TakeFirst()
    )
    keywords = scrapy.Field(
        output_processor=Identity()
    )
    posted_at = scrapy.Field(
        output_processor=TakeFirst()
    )
    resource = scrapy.Field(
        output_processor=TakeFirst()
    )
    url_start = scrapy.Field(
        output_processor=TakeFirst()
    )
    url_last = scrapy.Field(
        output_processor=TakeFirst()
    )
    video_count = scrapy.Field(
        output_processor=TakeFirst()
    )

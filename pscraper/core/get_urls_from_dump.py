import json
import pickle
import os


def get_links(file_name):
    with open(file_name, 'rb') as f:
        data = pickle.load(f)

    links = []
    for row in data:
        links.append(row['url'])
    return links

if __name__ == '__main__':
    result = []
    onlyfiles = [f for f in os.listdir(os.getcwd()) if os.path.isfile(os.path.join(os.getcwd(), f)) and 'bb' in f]
    for file_name in onlyfiles:
        print(file_name)
        result.extend(get_links(file_name))

    with open('downloaded_links.json', 'w') as f:
        json.dump(result, f)

    print('OK')

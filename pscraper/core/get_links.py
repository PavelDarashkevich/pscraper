from scrapy.crawler import CrawlerProcess, Crawler
from pscraper.core.database.models import Resources, DoesNotExist
from pscraper.core.spiders import SpiderType, get_spiders


def get_resource(module_name):
    return Resources.get(module=module_name)


def get_links(module_names, config):
    settings = {
        'ITEM_PIPELINES': {
            # "onliner.pipelines.JsonFileStorePipeline.JsonFileStorePipeline": 100,
            "pscraper.pipelines.MySqlPipelines.MySqlGetLinksPipeline": 100,
            # "pscraper.pipelines.JsonFileStorePipeline.JsonFileStorePipeline": 100,
        },
        'SPIDER_MIDDLEWARES': {
            'pscraper.middlewares.middlewares.DefaultMiddleware': 543,
        },
        'LOG_LEVEL': 'INFO',
        'ROBOTSTXT_OBEY': True,
        'CONCURRENT_REQUESTS': 128,

        "PSCRAPER_CONFIG": config
    }

    process = CrawlerProcess(settings)

    for module_name in module_names:
        spider_name, spider_cls = get_spiders(module_name, SpiderType.GET_LINKS)
        process.crawl(spider_cls)
        # process.crawl(spider_cls)
    process.start()
    process.stop()


from pscraper.core.database.models import *
from pscraper.core.config import DatabaseConfig


class DatabaseManager(object):
    class BaseModel(Model):
        pass

    models = [
        Resources, Articles, Categories, Publications,
        Authors, AuthorsPublications, Keywords, KeywordsPublications
    ]

    def __init__(self, db_config):
        """
        :type db_config DatabaseConfig
        :param db_config: 
        """
        self.database = MySQLDatabase(
            database=db_config.db_name,
            user=db_config.user_name,
            password=db_config.password)

    def create_tables(self):
        self.database.create_tables(self.models)

    def get_context(self):
        return Using(self.database, self.models)

if __name__ == '__main__':
    database = MySQLDatabase('supernova', **{'password': 'mysql', 'user': 'mysql'})
    database_manager = DatabaseManager(database)

    with database_manager.get_context():
        print(Keywords.select().count())

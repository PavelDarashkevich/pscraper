import datetime

from peewee import *


class BaseModel(Model):
    class Meta:
        database = None


class Resources(BaseModel):
    created_at = DateTimeField(null=True)
    domain = CharField()
    module = CharField()
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'resources'


class Articles(BaseModel):
    created_at = DateTimeField(null=True, default=datetime.datetime.now)
    error_description = TextField(null=True)
    http_response_status = IntegerField(null=True)
    posted_at = DateTimeField(null=True)
    resource = ForeignKeyField(db_column='resource_id', rel_model=Resources, to_field='id')
    state = CharField()
    static_processed = IntegerField()
    updated_at = DateTimeField(null=True)
    url = CharField()
    with_api = IntegerField()

    class Meta:
        db_table = 'articles'
        indexes = (
            (('resource', 'url'), True),
        )


class Categories(BaseModel):
    created_at = DateTimeField(null=True)
    name = CharField()
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'categories'


class Publications(BaseModel):
    article = ForeignKeyField(db_column='article_id', rel_model=Articles, to_field='id',
                              unique=True)
    authors_text = CharField(null=True)
    category = ForeignKeyField(db_column='category_id', null=True, rel_model=Categories,
                               to_field='id')
    category_text = CharField(null=True)
    content = TextField(null=True)
    created_at = DateTimeField(null=True)
    image_count = IntegerField(null=True)
    internal = CharField(db_column='internal_id', null=True)
    is_text = IntegerField(null=True)
    is_weekend = IntegerField(null=True)
    keywords_text = CharField(null=True)
    lead = CharField(null=True)
    link_count = IntegerField(null=True)
    meta_tags = CharField(null=True)
    posted_at = DateTimeField(null=True)
    posted_weekday = IntegerField(null=True)
    resource = ForeignKeyField(db_column='resource_id', null=True, rel_model=Resources,
                               to_field='id')
    slug = CharField(null=True)
    time_delta = IntegerField(null=True)
    title = CharField(null=True)
    top_post = IntegerField(null=True)
    updated_at = DateTimeField(null=True)
    url = CharField(null=True)
    video_count = IntegerField(null=True)

    class Meta:
        db_table = 'publications'


class Authors(BaseModel):
    created_at = DateTimeField(null=True)
    email = CharField(null=True)
    facebook = CharField(null=True)
    name = CharField()
    resource = ForeignKeyField(db_column='resource_id', rel_model=Resources, to_field='id')
    twitter = CharField(null=True)
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'authors'


class AuthorsPublications(BaseModel):
    author = ForeignKeyField(db_column='author_id', null=True, rel_model=Authors,
                             to_field='id')
    created_at = DateTimeField(null=True)
    publication = ForeignKeyField(db_column='publication_id', null=True,
                                  rel_model=Publications, to_field='id')
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'authors_publications'


class Keywords(BaseModel):
    created_at = DateTimeField(null=True)
    name = CharField()
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'keywords'


class KeywordsPublications(BaseModel):
    created_at = DateTimeField(null=True)
    keyword = ForeignKeyField(db_column='keyword_id', null=True, rel_model=Keywords,
                              to_field='id')
    publication = ForeignKeyField(db_column='publication_id', null=True,
                                  rel_model=Publications, to_field='id')
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'keywords_publications'

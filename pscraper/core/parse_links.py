from scrapy.crawler import CrawlerProcess


def parse_links(spider, module_name, database_manager):
    settings = {'NEWSPIDER_MODULE': 'onliner.spiders',
                'SPIDER_MODULES': ['onliner.spiders'],
                'ITEM_PIPELINES': {
                    "onliner.pipelines.MySqlPipelines.MySqlParseLinksPipeline": 100
                },
                'BOT_NAME': 'onliner',
                'LOG_LEVEL': 'INFO',
                'ROBOTSTXT_OBEY': True,
                'CONCURRENT_REQUESTS': 128}

    with database_manager.get_context():
        process = CrawlerProcess(settings)
        process.crawl(spider, resource=database_manager.Resources.select(database_manager.Resources.id).where(
            database_manager.Resources.module == module_name).get())
        process.start()
        process.stop()

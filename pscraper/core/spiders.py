import enum
import importlib
import inspect
from abc import abstractmethod

from scrapy.spiders import Spider


class SpiderType(enum.IntEnum):
    GET_LINKS = 1
    PARSE_LINKS = 2


class PScraperSpider(Spider):
    name = None
    spider_type = None
    domain = None

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)

        attrs = ['name', 'spider_type', 'domain']
        for attr in attrs:
            if getattr(self, attr, None) is None:
                raise ValueError("%s is None!" % attr)

    @abstractmethod
    def parse(self, response):
        raise NotImplementedError('parse')


class GetLinksSpider(PScraperSpider):
    spider_type = SpiderType.GET_LINKS

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @abstractmethod
    def parse(self, response):
        raise NotImplementedError('parse')


class ParseLinksSpider(PScraperSpider):
    spider_type = SpiderType.PARSE_LINKS

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @abstractmethod
    def parse(self, response):
        raise NotImplementedError('parse')


def get_spiders(module_name, spider_type):
    package_name = 'pscraper.spiders.' + module_name

    module = importlib.import_module(name=package_name)
    members = inspect.getmembers(module)

    for member_name, member in members:
        full_member_name = package_name + "." + member_name

        if getattr(member, 'spider_type', None) == spider_type:
            if member.__module__ == package_name:
                return full_member_name, member

    return None

if __name__ == '__main__':
    print(get_spiders('tut_by', SpiderType.PARSE_LINKS))
    print(get_spiders('onliner_by', SpiderType.PARSE_LINKS))
    print(get_spiders('tut_by', SpiderType.GET_LINKS))
    print(get_spiders('onliner_by', SpiderType.GET_LINKS))
    print(get_spiders('raw_spider', SpiderType.GET_LINKS))
